﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project
{
    // Code on this page uses Christine's school system example as reference
    public partial class Webpage : System.Web.UI.Page
    {
        private string page_id
        {
            get { return Request.QueryString["page_id"]; }
        }

        private string page_details_query = "SELECT page_title, page_content, page_author, published, publish_date " +
                                            "FROM webpages ";

        protected void Page_Load(object sender, EventArgs e)
        {
            // If no page_id is specified
            if (page_id == "" || page_id == null)
            {
                page_title.InnerHtml = "This page does not exist.";
                update_link.Visible = false;
                delete_webpage_button.Visible = false;
            }
            else
            {
                // Linking page id with the select query
                page_details_query += " WHERE page_id = " + page_id;
                page_details_select.SelectCommand = page_details_query;

                DataView page_details_view = (DataView)page_details_select.Select(DataSourceSelectArguments.Empty);
                string title = page_details_view[0]["page_title"].ToString();
                this.Title = title;

                // If the page is not published information about the page will not be displayed
                if (page_details_view[0]["published"].ToString() == "False")
                {
                    page_title.InnerHtml = "You are trying to access an unpublished page.";
                    page_author.Visible = false;
                    published_date.Visible = false;
                    update_link.Visible = false;
                    delete_webpage_button.Visible = false;
                    page_content.Visible = false;
                }
                else
                {
                    page_title.InnerHtml = title;
                    page_author.InnerHtml = "Author: " + page_details_view[0]["page_author"].ToString();
                    published_date.InnerHtml = "Published on: " + page_details_view[0]["publish_date"];
                    page_content.InnerHtml = page_details_view[0]["page_content"].ToString();

                    // Update Link, specifying to return to this current page
                    update_link.HRef = "CreateOrUpdateWebpage.aspx?cmd=update&orig_page=Webpage.aspx?page_id=" + page_id
                           + "&page_id=" + page_id;
                }
            }
        }

        // If the user chooses to delete this webpage
        protected void Delete_Webpage(object sender, EventArgs e)
        {
            string delete_query = "DELETE FROM webpages WHERE page_id = " + page_id;

            webpage_delete.DeleteCommand = delete_query;
            webpage_delete.Delete();

            Response.Redirect("WebpageManager.aspx");
        }
    }
}