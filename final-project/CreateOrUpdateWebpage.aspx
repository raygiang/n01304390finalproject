﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateOrUpdateWebpage.aspx.cs" Inherits="final_project.CreateWebpage" %>

<asp:Content ID="AddNewWebpage" ContentPlaceHolderID="MainContent" runat="server">
    <%-- data source object for creating a page --%>
    <asp:SqlDataSource runat="server" id="insert_new_webpage"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>

    <%-- data source for grabbing the existing page information when updating a page --%>
    <asp:SqlDataSource 
        runat="server"
        id="view_existing_select"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>

    <%-- data source object for updating the page information --%>
    <asp:SqlDataSource 
        runat="server"
        id="update_info"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>

    <h2 id="page_heading" runat="server"></h2>

    <%-- Field for page title --%>
    <div class="row">
        <asp:label runat="server" AssociatedControlID="page_title_box">Page Title:</asp:label>
        <asp:TextBox id="page_title_box" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title_box"
            ErrorMessage="Please enter a title for your page"
            ForeColor="Red">
        </asp:RequiredFieldValidator>
    </div>

    <%-- Field for page author --%>
    <div class="row">
        <asp:label runat="server" AssociatedControlID="page_author_box">Page Author:</asp:label>
        <asp:TextBox id="page_author_box" runat="server"></asp:TextBox>
    </div>

    <%-- Field for page content --%>
    <div class="row">
        <asp:label runat="server" AssociatedControlID="page_content_box">Page Content:</asp:label>
        <asp:TextBox id="page_content_box" 
            runat="server" 
            TextMode="multiline">
        </asp:TextBox>
    </div>

    <%-- Buttons for adding or updating the webpage, one will always be hidden in the code behind --%>
    <asp:Button id="add_page_button" runat="server" OnClick="Add_Webpage" Text="Add a New Webpage"/>
    <asp:Button id="edit_page_button" runat="server" OnClick="Edit_Webpage" Text="Update the Webpage"/>
</asp:Content>