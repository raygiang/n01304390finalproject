﻿<%@ Page Title="Manage Webpages" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebpageManager.aspx.cs" Inherits="final_project.ManageWebpages" %>

<asp:Content ID="WebpagesList" ContentPlaceHolderID="MainContent" runat="server">
    <%-- data source object for deleting a page --%>
    <asp:SqlDataSource 
        runat="server"
        id="webpage_delete"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>

    <%-- data source object used to display a list of all webpages --%>
    <asp:SqlDataSource runat="server"
        id="webpages_select"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>

    <%-- data source object used to publish or unpublish a webpage --%>
    <asp:SqlDataSource runat="server"
        id="publish_unpublish_query"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>

    <h2>Webpage Manager</h2>

    <a runat="server" href="~/CreateOrUpdateWebpage.aspx?cmd=create">Create a New Webpage</a>

    <%-- The search form to filter through table results --%>
    <fieldset>
        <legend>Filter Results</legend>

        <div class="row">
            <asp:label runat="server" AssociatedControlID="page_title_search_box">Search by Title: </asp:label>
            <asp:TextBox runat="server" ID="page_title_search_box"></asp:TextBox>
        </div>

        <div class="row">
            <asp:label runat="server" AssociatedControlID="page_author_search_box">Search by Author: </asp:label>
            <asp:TextBox runat="server" ID="page_author_search_box"></asp:TextBox>
        </div>

        <div class="row">
            <asp:Label runat="server" AssociatedControlID="result_sort_field">Sort the Results By: </asp:Label>
            <asp:DropDownList runat="server" ID="result_sort_field">
                <asp:ListItem Value="title" Text="By Title"></asp:ListItem>
                <asp:ListItem Value="author" Text="By Author"></asp:ListItem>
                <asp:ListItem Value="publish_date" Text="By Date Published"></asp:ListItem>
            </asp:DropDownList>
        </div>

        <div class="row">
            <asp:RadioButtonList runat="server" ID="sort_order" RepeatDirection="Horizontal">
                <asp:ListItem Text="ascending" selected="True">Ascending Order</asp:ListItem>
                <asp:ListItem Text="descending">Descending Order</asp:ListItem>
            </asp:RadioButtonList>
        </div>

        <asp:Button id="search_button" runat="server" OnClick="Webpages_Search" Text="Filter"/>
    </fieldset>

    <%-- Placeholder for the webpages table --%>
    <asp:DataGrid id="webpages_table" runat="server" >
    </asp:DataGrid>

</asp:Content>
