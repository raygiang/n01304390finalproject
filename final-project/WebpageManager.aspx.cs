﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project
{
    // Code on this page uses Christine's school system example as reference
    public partial class ManageWebpages : System.Web.UI.Page
    {
        // cmd is used to specify if a delete, publish, or unpublish was clicked
        private string cmd
        {
            get { return Request.QueryString["cmd"]; }
        }

        public int page_id
        {
            get { return Convert.ToInt32(Request.QueryString["page_id"]); }
        }

        private string page_basequery = "SELECT page_id, " +
                                               "page_title AS 'Page Title', " +
                                               "page_author AS 'Author', " +
                                               "publish_date AS 'Date Published', " +
                                               "published " +
                                        "FROM webpages ";

        protected void Page_Load(object sender, EventArgs e)
        {
            // Execute the appropriate function if cmd is specified
            if (cmd == "publish")
            {
                Publish_Webpage();
            }
            else if (cmd == "unpublish")
            {
                Unpublish_Webpage();
            }
            else if (cmd == "delete")
            {
                Delete_Webpage();
            }

            // Display information about the webpages table after processing is done by the rendering loop
            string ordered_basequery = page_basequery + "ORDER BY page_id ";
            webpages_select.SelectCommand = ordered_basequery;
            webpages_table.DataSource = ManageWebpages_Manual_Bind(webpages_select);
            webpages_table.DataBind();
        }

        protected DataView ManageWebpages_Manual_Bind(SqlDataSource src)
        {
            DataTable webpages_table;
            DataView table_view;

            webpages_table = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            // Adding columns to our table
            webpages_table.Columns.Add("Publish/Unpublish");
            webpages_table.Columns.Add("Edit Page");
            webpages_table.Columns.Add("Delete Page");

            // Rendering loop to manipulate the information we want to show in our table
            foreach (DataRow table_row in webpages_table.Rows)
            {
                // Create links leading to individual pages
                table_row["Page Title"] =
                    "<a href=\"Webpage.aspx?page_id="
                    + table_row["page_id"] + "\">"
                    + table_row["Page Title"]
                    + "</a>";

                // Create publish or unpublish links
                if (table_row["published"].ToString() == "False")
                {
                    table_row["Publish/Unpublish"] = "<a href=\"WebpageManager.aspx?cmd=publish&page_id="
                    + table_row["page_id"] + "\">"
                    + "Publish"
                    + "</a>";
                }
                else
                {
                    table_row["Publish/Unpublish"] = "<a href=\"WebpageManager.aspx?cmd=unpublish&page_id="
                    + table_row["page_id"] + "\">"
                    + "Unpublish"
                    + "</a>";
                }
                
                // Create links to delete individual pages
                table_row["Delete Page"] = "<a href=\"WebpageManager.aspx?cmd=delete&page_id="
                    + table_row["page_id"] + "\" "
                    + "onclick=\"if (!confirm('Are you sure you want to remove this page?')) return false; \">"
                    + "Delete"
                    + "</a>";

                // Create links to edit individual pages
                table_row["Edit Page"] = "<a href=\"CreateOrUpdateWebpage.aspx?cmd=update&orig_page=WebpageManager.aspx&page_id="
                    + table_row["page_id"] + "\">"
                    + "Edit"
                    + "</a>";
            }

            // Remove columns used for processing but we don't want displayed
            webpages_table.Columns.Remove("page_id");
            webpages_table.Columns.Remove("published");
            table_view = webpages_table.DefaultView;

            return table_view;
        }

        // function used to delete a webpage
        protected void Delete_Webpage()
        {
            string delete_query = "DELETE FROM webpages WHERE page_id =" + page_id;

            webpage_delete.DeleteCommand = delete_query;
            webpage_delete.Delete();
            Response.Redirect("WebpageManager.aspx");
        }

        // function used to publish a webpage
        protected void Publish_Webpage()
        {
            string publish_query = "UPDATE webpages " +
                                   "SET published = 'True', " +
                                   "publish_date = GETDATE() " +
                                   "WHERE page_id = " + page_id;
            publish_unpublish_query.UpdateCommand = publish_query;
            publish_unpublish_query.Update();
            Response.Redirect("WebpageManager.aspx");
        }

        // function used to unpublish a webpage
        protected void Unpublish_Webpage()
        {
            string unpublish_query = "UPDATE webpages " +
                                   "SET published = 'False', " +
                                   "publish_date = NULL " +
                                   "WHERE page_id = " + page_id;
            publish_unpublish_query.UpdateCommand = unpublish_query;
            publish_unpublish_query.Update();
            Response.Redirect("WebpageManager.aspx");
        }

        // function used to search the webpages table
        protected void Webpages_Search(object sender, EventArgs e)
        {
            string search_query = page_basequery + "WHERE (1=1) ";

            string title_search_text = page_title_search_box.Text;
            string author_search_text = page_author_search_box.Text;

            if (title_search_text != "")
            {
                search_query += " AND (page_title LIKE '%" + title_search_text + "%') ";
            }
            if (author_search_text != "")
            {
                search_query += " AND (page_author LIKE '%" + author_search_text + "%') ";
            }

            if (result_sort_field.SelectedValue == "title")
            {
                search_query += "ORDER BY page_title ";
            }
            else if (result_sort_field.SelectedValue == "author")
            {
                search_query += "ORDER BY page_author ";
            }
            else if (result_sort_field.SelectedValue == "publish_date")
            {
                search_query += "ORDER BY publish_date ";
            }
            else
            {
                search_query += "ORDER BY page_id ";
            }

            if (sort_order.SelectedValue == "Descending Order")
            {
                search_query += "DESC ";
            }

            webpages_select.SelectCommand = search_query;
            webpages_table.DataSource = ManageWebpages_Manual_Bind(webpages_select);
            webpages_table.DataBind();
        }
    }
}