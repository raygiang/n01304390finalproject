﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project.User_Controls
{
    // Code on this page uses Christine's school system example as reference
    public partial class PageMenu : System.Web.UI.UserControl
    {
        // Query to get information from the database used to generate the menu
        private string display_pages_query = "SELECT page_id, page_title, published " +
                                             "FROM webpages " +
                                             "ORDER BY page_id";

        private string menu_string = "<h3>Your Webpages</h3> ";

        protected void Page_Load(object sender, EventArgs e)
        {
            display_webpages_select.SelectCommand = display_pages_query;
            Display_Menu(display_webpages_select);
        }

        protected void Display_Menu(SqlDataSource src)
        {
            // Christine's Code
            DataView webpages_view = (DataView)src.Select(DataSourceSelectArguments.Empty);

            // Iterate through the the list of all webpages and create links to the pages only if they are published
            menu_string += "<ul>";
            for (int i = 0; i < webpages_view.Count; i++)
            {
                if (webpages_view[i]["published"].ToString() == "True") {
                    menu_string += "<li><a href=\"Webpage.aspx?page_id=" + webpages_view[i]["page_id"] + "\">" +
                        webpages_view[i]["page_title"] + "</a></li> ";
                }
            }
            menu_string += "</ul>";

            pages_menu.InnerHtml = menu_string;
        }
    }
}