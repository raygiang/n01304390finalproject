﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageMenu.ascx.cs" Inherits="final_project.User_Controls.PageMenu" %>

<asp:SqlDataSource 
    runat="server" 
    ID="display_webpages_select"
    ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
</asp:SqlDataSource>

<%--Div used as placeholder for webpages menu--%>
<div id="pages_menu" class="page-wrapper" runat="server">
</div>