﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project
{
    // Code on this page uses Christine's school system example as reference
    public partial class CreateWebpage : System.Web.UI.Page
    {
        // cmd is used to specify if we are doing an update or creating a new page
        public string cmd
        {
            get { return Request.QueryString["cmd"]; }
        }

        // orig_page is the path we redirect to after an update is complete
        public string orig_page
        {
            get { return Request.QueryString["orig_page"]; }
        }

        public int page_id
        {
            get { return Convert.ToInt32(Request.QueryString["page_id"]); }
        }

        private string insert_page_query = "INSERT INTO webpages" +
            "(page_title, page_author, page_content) VALUES ";

        protected void Page_Load(object sender, EventArgs e)
        {
            // Change information displayed based on whether we are creating or updating
            if (cmd == "create")
            {
                this.Title = "Create Page";
                page_heading.InnerHtml = "Create a Page";
                edit_page_button.Visible = false;
            }
            else if (cmd == "update")
            {
                this.Title = "Update Page";
                page_heading.InnerHtml = "Update your Page";
                add_page_button.Visible = false;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            // If we are updating, display existing content
            if (cmd == "update")
            {
                base.OnPreRender(e);

                DataRowView page_row = GetCurrentInfo();
                if (page_row == null)
                {
                    return;
                }
                page_title_box.Text = page_row["page_title"].ToString();
                page_author_box.Text = page_row["page_author"].ToString();
                page_content_box.Text = page_row["page_content"].ToString();
            }
        }

        // Create a new webpage and redirect back to the Webpage Manager
        protected void Add_Webpage(object sender, EventArgs e)
        {
            string page_title_to_insert = page_title_box.Text;
            string page_author_to_insert = page_author_box.Text;
            string page_content_to_insert = page_content_box.Text;

            insert_page_query += "('" + page_title_to_insert + "', '" + page_author_to_insert + "', '" + page_content_to_insert + "')";

            insert_new_webpage.InsertCommand = insert_page_query;
            insert_new_webpage.Insert();

            Response.Redirect("WebpageManager.aspx");
        }

        // Update a webpage and redirect to the page where update was clicked
        protected void Edit_Webpage(object sender, EventArgs e)
        {
            string new_page_title = page_title_box.Text;
            string new_page_author = page_author_box.Text;
            string new_page_content = page_content_box.Text;

            string edit_query = "UPDATE webpages " +
                                "SET page_title = '" + new_page_title + "', " +
                                "page_author = '" + new_page_author + "', " +
                                "page_content = '" + new_page_content + "' " +
                                "WHERE page_id = " + page_id;
            update_info.UpdateCommand = edit_query;
            update_info.Update();
            Response.Redirect(orig_page);
        }

        // Get the existing information from the database associated with this page_id
        protected DataRowView GetCurrentInfo()
        {
            string select_query = "SELECT * FROM webpages where page_id=" + page_id;
            view_existing_select.SelectCommand = select_query;

            // Christine's Code
            DataView webpage_view = (DataView)view_existing_select.Select(DataSourceSelectArguments.Empty);
            if (webpage_view.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView webpage_row = webpage_view[0];

            return webpage_row;
        }
    }
}