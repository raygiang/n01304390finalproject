﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Webpage.aspx.cs" Inherits="final_project.Webpage" %>

<asp:Content ID="SinglePageView" ContentPlaceHolderID="MainContent" runat="server">
    <%-- data source object for deleting a page --%>
    <asp:SqlDataSource 
        runat="server"
        id="webpage_delete"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>

    <%-- data source object which will show the page details --%>
   <asp:SqlDataSource runat="server"
        id="page_details_select"
        ConnectionString="<%$ ConnectionStrings:webpages_sql_con %>">
    </asp:SqlDataSource>
    
    <%-- Placeholders for the title, author, and date of publish --%>
    <h2 id="page_title" runat="server"></h2>
    <div id="page_author" runat="server"></div>
    <div id="published_date" runat="server"></div>


    <%-- Update and Delete buttons, Delete button is based on Christine's example --%>
    <div class="button-row">
        <a id="update_link" runat="server">Update Webpage</a>
    </div>
    <div class="button-row">
        <asp:Button runat="server" id="delete_webpage_button"
            OnClick="Delete_Webpage"
            OnClientClick="if(!confirm('Are you sure you want to remove this page?')) return false;"
            Text="Delete this Page" />
    </div>

    <%-- Placeholder for the page content --%>
    <div id="page_content" runat="server">
    </div>
</asp:Content>
