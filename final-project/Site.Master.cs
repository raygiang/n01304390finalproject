﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Toggle_Webpage_Menu(object sender, EventArgs e)
        {
            // Switches the webpage menu styling if the toggle button is clicked
            if (webpages_navigation.Attributes["class"] == null)
            {
                webpages_navigation.Attributes.Add("class", "alt-menu");
            }
            else
            {
                webpages_navigation.Attributes.Remove("class");
            }
        }
    }
}