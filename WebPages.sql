﻿CREATE TABLE "webpages" 
(
	"page_id" INT IDENTITY PRIMARY KEY, 
	"page_title" VARCHAR(50) NOT NULL, 
	"page_content" VARCHAR(MAX) NULL,
	"pubished" BIT NOT NULL DEFAULT 0,
    "publish_date" DATETIME NULL,
    "page_author" VARCHAR(50) NULL
);